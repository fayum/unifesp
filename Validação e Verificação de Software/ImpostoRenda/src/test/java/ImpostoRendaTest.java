import org.junit.Assert;
import org.junit.Test;

public class ImpostoRendaTest {

    @Test
    public void calculaIR() throws Exception {

        Assert.assertEquals(0, ImpostoRenda.calculaIR(1000.0),0.0001);
        Assert.assertEquals(150, ImpostoRenda.calculaIR(2000.0),0.0001);
        Assert.assertEquals(350, ImpostoRenda.calculaIR(3000.0),0.0001);
        Assert.assertEquals(550, ImpostoRenda.calculaIR(4000.0),0.0001);
        Assert.assertEquals(0, ImpostoRenda.calculaIR(0.0),0.0001);
    }

    @Test(expected = Exception.class)
    public void testNotANumber() throws Exception {
        ImpostoRenda.calculaIR(Double.NaN);
    }

    @Test(expected = Exception.class)
    public void testeNegativeSalary() throws Exception {
        ImpostoRenda.calculaIR(-1.0);
    }

}
