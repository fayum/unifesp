import java.util.regex.Pattern;

public class StringSum {

    public static String Sum(String num1, String num2) {
        int parsedNum1 = StringSum.ehNumeroNatural(num1) ? Integer.parseInt(num1) : 0;
        int parsedNum2 = StringSum.ehNumeroNatural(num2) ? Integer.parseInt(num2) : 0;

        return String.valueOf(parsedNum1 + parsedNum2);
    }

    public static boolean ehNumeroNatural(String num) {
        if (num == null || num.equals("0")) return false;
        Pattern pattern = Pattern.compile("\\d+");
        return pattern.matcher(num).matches();
    }
}
