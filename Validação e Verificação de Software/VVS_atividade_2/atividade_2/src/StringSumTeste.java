import org.junit.Assert;
import org.junit.Test;

public class StringSumTeste {

    public StringSumTeste() {
    }

    @Test
    public void testeGetClassificacao() {
        Assert.assertEquals("3",StringSum.Sum("1","2"));
        Assert.assertEquals("10",StringSum.Sum("3","7"));
        Assert.assertEquals("7",StringSum.Sum("-3","7"));
        Assert.assertEquals("0",StringSum.Sum("-3","-7"));
        Assert.assertEquals("0",StringSum.Sum(null,"-7"));
        Assert.assertEquals("0",StringSum.Sum("-3",null));
        Assert.assertEquals("3",StringSum.Sum("a","3"));
        Assert.assertEquals("7",StringSum.Sum("7","b"));
    }

    @Test
    public void ehNumeroNatural() {
        Assert.assertEquals(true,StringSum.ehNumeroNatural("1"));
        Assert.assertEquals(true,StringSum.ehNumeroNatural("11"));
        Assert.assertEquals(false,StringSum.ehNumeroNatural("-1"));
        Assert.assertEquals(false,StringSum.ehNumeroNatural(""));
        Assert.assertEquals(false,StringSum.ehNumeroNatural(null));
        Assert.assertEquals(false,StringSum.ehNumeroNatural("1.1"));
        Assert.assertEquals(false,StringSum.ehNumeroNatural("1.0"));
        Assert.assertEquals(false,StringSum.ehNumeroNatural("a"));
    }
}
