public class Game {

    public static final int idxLastGameFrame = 9;
    private int frames[][] = new int[11][2];
    private int framesIdx = 0;
    private int frameRollIdx = 0;

    public void roll(int pinsKnockedDown) throws Exception {
        if(pinsKnockedDown < 0) {
            throw new Exception("Numbers of pins knocked down lower than expected");
        }
        if(this.framesIdx <= this.idxLastGameFrame ||
                ((this.framesIdx == this.idxLastGameFrame + 1) && isFrameSpare(idxLastGameFrame)) ||
                ((this.framesIdx == this.idxLastGameFrame + 1) && isFrameStrike(idxLastGameFrame))) {
            this.frames[this.framesIdx][this.frameRollIdx] = pinsKnockedDown;
            this.adjustIndexes();
        } else {
            throw new Exception("All rows completed");
        }
    }

    public int getScore() {
        int score = 0;

        for (int i = 0; i <= idxLastGameFrame; i++) {
            score += this.frames[i][0] + this.frames[i][1];

            if (this.isFrameStrike(i)) {
                score += this.getStrikeBonus(i);
            } else if (this.isFrameSpare(i)) {
                score += this.getSpareBonus(i);
            }
        }
        return score;
    }

    private boolean isFrameSpare(int frameIndex) {
        return this.frames[frameIndex][0] + this.frames[frameIndex][1] == 10;
    }

    private boolean isFrameStrike(int frameIndex) {
        return this.frames[frameIndex][0] == 10;
    }

    private int getSpareBonus(int frameIndex) {
        return this.frames[frameIndex+1][0];
    }

    private int getStrikeBonus(int frameIndex) {

        if ((frameIndex == idxLastGameFrame) || (frameIndex < idxLastGameFrame && this.frames[frameIndex+1][0] != 10)) {
            return this.frames[frameIndex+1][0] + this.frames[frameIndex+1][1];
        } else {
            return this.frames[frameIndex+1][0] + this.frames[frameIndex+2][0];
        }
    }

    private void adjustIndexes() {
        if ((this.frameRollIdx == 1 || this.isFrameStrike(this.framesIdx)) && (this.framesIdx <= idxLastGameFrame)) {
            this.frameRollIdx = 0;
            this.framesIdx ++;
        } else {
            this.frameRollIdx = 1;
        }
    }
}
