import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GameTest {

    private Game game;

    @Before
    public void init() {
        this.game = new Game();
    }

    @Test
    public void gutterGameTest() throws Exception {
        this.rollRepetitive(20, 0);
        Assert.assertEquals(0, game.getScore());
    }

    @Test
    public void allOnesGameTest() throws Exception {
        rollRepetitive(20, 1);
        Assert.assertEquals(20, game.getScore());
    }

    @Test
    public void spareSimpleTest() throws Exception {
        this.rollSpare();
        this.game.roll(5);
        this.rollRepetitive(17, 0);
        Assert.assertEquals(20, game.getScore());
    }

    @Test
    public void spareLastFrameTest() throws Exception {
        this.rollRepetitive(18, 0);
        this.rollSpare();
        this.game.roll(5);
        Assert.assertEquals(15, game.getScore());
    }

    @Test
    public void strikeTest() throws Exception {
        this.rollStrike();
        this.game.roll(5);
        this.game.roll(3);
        this.rollRepetitive(16, 0);
        Assert.assertEquals(26, game.getScore());
    }

    @Test
    public void perfectGameTest() throws Exception {
        this.rollRepetitive(12, 10);
        Assert.assertEquals(300, game.getScore());
    }

    @Test(expected = Exception.class)
    public void moreRollsThanExpected() throws Exception {
        for (int i = 0; i < 12; i++) {
            this.rollRepetitive(12, 1);
        }
    }

    @Test(expected = Exception.class)
    public void negativePinsKnockedDown() throws Exception {
        this.game.roll(-1);
    }

    private void rollRepetitive(int rolls, int pins) throws Exception {
        for(int i = 0; i< rolls; i++) {
            game.roll(pins);
        }
    }

    private void rollSpare() throws Exception {
        this.game.roll(9);
        this.game.roll(1);
    }

    private void rollStrike() throws Exception {
        this.game.roll(10);
    }

}
