import org.junit.Assert;
import org.junit.Test;


public class SillyPascalVarTest {

        @Test
        public void isValid() {
                Assert.assertEquals(true, SillyPascalVar.isValid("test1"));
                Assert.assertEquals(true, SillyPascalVar.isValid("t"));
                Assert.assertEquals(true, SillyPascalVar.isValid("test12"));

                Assert.assertEquals(false, SillyPascalVar.isValid(null));
                Assert.assertEquals(false, SillyPascalVar.isValid(""));
                Assert.assertEquals(false, SillyPascalVar.isValid("1test"));
                Assert.assertEquals(false, SillyPascalVar.isValid("tes@t"));
                Assert.assertEquals(false, SillyPascalVar.isValid("test123"));
        }

}
