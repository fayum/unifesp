import java.util.regex.Pattern;


public class SillyPascalVar {

        public static Boolean isValid (String value) {
                if (value == null)
                        return false;
                Pattern pattern = Pattern.compile("[a-zA-Z]([a-zA-Z0-9]{1,5})?");
                return pattern.matcher(value).matches();
        }
}
