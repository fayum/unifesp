/****************************************************/
/* File: cminus.l                                   */
/* Lex specification for C-                         */
/****************************************************/

%{
#include "TOKENS.h"
#include <stdio.h>
int linha = 1;
%}

digit       [0-9]
number      {digit}+
letter      [a-zA-Z]
identifier  {letter}+
newline     \n
whitespace  [ \t]+

%%

"if"            {return IF;}
"else"          {return ELSE;}
"return"        {return RETURN;}
"void"          {return VOID;}
"while"         {return WHILE;}
"int"           {return INT;}
"+"             {return PLUS;}
"-"             {return MINUS;}
"*"             {return MULTIPLICATION;}
"/"             {return DIVISION;}
"<"             {return LESSTHAN;}
"<="            {return LESSTHANEQUAL;}
">"             {return MORETHAN;}
">="            {return MORETHANEQUAL;}
"=="            {return EQUAL;}
"!="            {return DIFFERENT;}
"="             {return ASSIGNMENT;}
";"             {return SEMICOLON;}
","             {return COLON;}
"("             {return LEFTPARENTHESES;}
")"             {return RIGHTPARENTHESES;}
"["             {return LEFTBRACKETS;}
"]"             {return RIGHTBRACKETS;}
"{"             {return LEFTKEYS;}
"}"             {return RIGHTKEYS;}
{number}        {return NUMBER;}
{identifier}    {return ID;}
{newline}       {linha++;}
{whitespace}    {/* skip whitespace */}
"/*"            { char c1, c2;
                  do
                  { c2 = c1;
                    c1 = input();
                    if (c1 == EOF) break;
                    if (c1 == '\n') linha++;
                  } while (c1 != '/' && c2 !='*');
                }
.               {return ERROR;}
<<EOF>>         {return END_OF_FILE;}

%%
int yywrap() {
   return 1;
}

int main(int argc, char *argv[])
{
  FILE *f_in;
  TypeOfToken current_token;
  if (argc == 2)
  {
    if(f_in = fopen(argv[1],"r"))  yyin = f_in;
    else  perror(argv[0]);
  }
  else  yyin = stdin;

  char *stringToPrint = "";
  int linhaAntiga = linha;
  while((current_token = yylex())!=END_OF_FILE)
  {
    //stringToPrint = toString(current_token);
    if(linha != linhaAntiga)
    {
      printf("\n%d %s", linha, toString(current_token));
      linhaAntiga = linha;
    }
    else
      printf(" %s", toString(current_token));

  } 
  yylex();
  return(0);
}

