typedef enum{
	IF, ELSE, INT, RETURN, VOID, WHILE,
	PLUS, MINUS, MULTIPLICATION, DIVISION,
	LESSTHANEQUAL, MORETHANEQUAL,
	MORETHAN, EQUAL, LESSTHAN, ASSIGNMENT,
	DIFFERENT, 
	LEFTPARENTHESES, RIGHTPARENTHESES,
	SEMICOLON, COLON,
	LEFTBRACKETS, RIGHTBRACKETS,
	LEFTKEYS, RIGHTKEYS,
	STARTCOM, ENDCOM,
	NUMBER, ID,
	ERROR, END_OF_FILE
} TypeOfToken;

char *toString(TypeOfToken current_token)
{
	char *stringToReturn = "";
	switch(current_token)
	{
		case IF:
			stringToReturn = "IF";
			break;
		case ELSE:
			stringToReturn = "ELSE";
			break;
		case INT:
			stringToReturn = "INT";
			break;
		case RETURN:
			stringToReturn = "RETURN";
			break;
		case VOID:
			stringToReturn = "VOID";
			break;
		case WHILE:
			stringToReturn = "WHILE";
			break;
		case PLUS:
			stringToReturn = "PLUS";
			break;
		case MINUS:
			stringToReturn = "MINUS";
			break;
		case LESSTHAN:
			stringToReturn = "LESSTHAN";
			break;
		case MORETHAN:
			stringToReturn = "MORETHAN";
			break;
		case LESSTHANEQUAL:
			stringToReturn = "LESSTHANEQUAL";
			break;
		case MORETHANEQUAL:
			stringToReturn = "MORETHANEQUAL";
			break;
		case EQUAL:
			stringToReturn = "EQUAL";
			break;
		case ASSIGNMENT:
			stringToReturn = "ASSIGNMENT";
			break;
		case DIFFERENT:
			stringToReturn = "DIFFERENT";
			break;
		case LEFTPARENTHESES:
			stringToReturn = "LEFTPARENTHESES";
			break;
		case RIGHTPARENTHESES:
			stringToReturn = "RIGHTPARENTHESES";
			break;
		case LEFTKEYS:
			stringToReturn = "LEFTKEYS";
			break;
		case RIGHTKEYS:
			stringToReturn = "RIGHTKEYS";
			break;
		case LEFTBRACKETS:
			stringToReturn = "LEFTBRACKETS";
			break;
		case RIGHTBRACKETS:
			stringToReturn = "RIGHTBRACKETS";
			break;
		case COLON:
			stringToReturn = "COLON";
			break;
		case SEMICOLON:
			stringToReturn = "SEMICOLON";
			break;
		case NUMBER:
			stringToReturn = "NUMBER";
			break;
		case ID:
			stringToReturn = "ID";
			break;
		case STARTCOM:
			stringToReturn = "STARTCOM";
			break;
		case ENDCOM:
			stringToReturn = "ENDCOM";
			break;
		case ERROR:
			stringToReturn = "ERROR";
			break;
		case END_OF_FILE:
			stringToReturn = "END_OF_FILE";
			break;
		default:
			stringToReturn = "Unknown Token";
			break;
	}
	return stringToReturn;
}